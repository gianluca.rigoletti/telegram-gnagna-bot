const bot = require("../bot")
const navigation = require("../navigation")
const db = require("../database")

bot.on("/start", async msg => {
  navigation.set(msg.chat.id, "start")
  await db.insertTelegramUser(msg.chat.id)
  const replyMarkup = bot.keyboard([
    ["➕ Add"],
    ["📃 List"],
    ["🚫 Remove"],
    ["⚙️ Settings"]
  ])
  bot.sendMessage(msg.chat.id, "Hello world", { replyMarkup })
})
