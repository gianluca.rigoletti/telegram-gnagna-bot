const bot = require("../bot")
const db = require("../database")

bot.on(/✅ Enable polling/, async msg => {
  await db.enablePolling(msg.chat.id)
  const replyMarkup = bot.keyboard([["✅ Enable polling"], ["🔙 Back"]])
  bot.sendMessage(msg.chat.id, "Polling enabled", { replyMarkup })
})
