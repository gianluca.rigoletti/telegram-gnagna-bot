const bot = require("../bot")
const navigation = require("../navigation")
const db = require("../database")

bot.on(/🔙 Back/, async msg => {
  const currentRoute = navigation.get(msg.chat.id)
  let replyMarkup
  console.log(currentRoute)
  const isPolling = await db.getPolling(msg.chat.id)
  switch (currentRoute) {
    case "settings":
      replyMarkup = bot.keyboard([
        ["➕ Add"],
        ["📃 List"],
        ["🚫 Remove"],
        ["⚙️ Settings"],
        isPolling ? [] : ["Poll"]
      ])
      navigation.set(msg.chat.id, "start")
      bot.sendMessage(msg.chat.id, "Back", { replyMarkup })
      break

    default:
      replyMarkup = bot.keyboard([
        ["➕ Add"],
        ["📃 List"],
        ["🚫 Remove"],
        ["⚙️ Settings"],
        isPolling ? [] : ["Poll"]
      ])
      break
  }
})
