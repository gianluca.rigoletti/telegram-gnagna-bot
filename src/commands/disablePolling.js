const bot = require("../bot")
const db = require("../database")

bot.on(/⛔️ Disable polling/, async msg => {
  await db.disablePolling(msg.chat.id)
  const replyMarkup = bot.keyboard([["✅ Enable polling"], ["🔙 Back"]])
  bot.sendMessage(msg.chat.id, "Polling disabled", { replyMarkup })
})
