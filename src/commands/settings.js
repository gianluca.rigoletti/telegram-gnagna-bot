const bot = require("../bot")
const navigation = require("../navigation")
const db = require("../database")

bot.on(/⚙️ Settings/, async msg => {
  navigation.set(msg.chat.id, "settings")
  const isPolling = await db.getPolling(msg.chat.id)
  console.log(isPolling)
  const replyText = isPolling ? "Polling is enabled" : "Polling is disabled"
  const replyKey = isPolling ? ["⛔️ Disable polling"] : ["✅ Enable polling"]
  const replyMarkup = bot.keyboard([replyKey, ["🔙 Back"]])
  bot.sendMessage(msg.chat.id, replyText, { replyMarkup })
})
