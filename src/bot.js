require("dotenv").config()
const TeleBot = require("telebot")

module.exports = new TeleBot(process.env.TOKEN)
