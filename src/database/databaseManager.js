const sqlite3 = require("sqlite3").verbose()
const _ = require("lodash")

class DatabaseManager {
  constructor(configs) {
    this.dbPath = configs.dbPath
    this.db = new sqlite3.Database(this.dbPath)
  }

  async insertTelegramUser(chatId) {
    return new Promise((res, rej) => {
      this.db.run(
        `INSERT OR IGNORE INTO telegramchat VALUES (null, ?, 1)`,
        chatId,
        err => {
          if (err) {
            rej(err)
          }
          this.db.get(
            `SELECT id FROM telegramchat WHERE chatid = ?`,
            chatId,
            (err, row) => {
              if (err) {
                rej(err)
              }
              res(row.id)
            }
          )
        }
      )
    })
  }

  async getTelegramUser(chatId) {
    return new Promise((res, rej) => {
      this.db.get(
        "SELECT * FROM telegramchat WHERE chatid = ? LIMIT 1",
        chatId,
        (err, row) => {
          if (err) {
            rej(err)
          }
          _.isEmpty(row) ? res(null) : res(row)
        }
      )
    })
  }

  async getAllTgUsers() {
    return new Promise((res, rej) => {
      this.db.all(
        "SELECT chatid FROM telegramchat ORDER BY id",
        [],
        (err, row) => {
          if (err) {
            rej(err)
          }
          if (_.isEmpty(row)) {
            res(null)
          }
          res(row.map(el => el.chatid))
        }
      )
    })
  }

  async insertInstagramUser(tgChatId, igUsername) {
    if (!tgChatId || !igUsername) {
      return Promise.reject(
        "insertInstagramUser expects tgChatId and igUsername"
      )
    }
    const tgUser = await this.getTelegramUser(tgChatId)
    return new Promise((res, rej) => {
      this.db.run(
        `INSERT OR IGNORE INTO instagramuser VALUES (null, ?)`,
        igUsername,
        err => {
          if (err) {
            rej(err)
          }
          this.db.get(
            "SELECT id FROM instagramuser WHERE username = ? LIMIT 1",
            igUsername,
            (err, row) => {
              if (err) {
                rej(err)
              }
              this.db.run(
                `INSERT INTO telegramchatinstagramuser VALUES (null, ?, ?)`,
                tgUser.id,
                row.id,
                err => {
                  if (err) {
                    rej(
                      `Already inserted instagram user ${JSON.stringify(err)}`
                    )
                  }
                }
              )
              res(row.id)
            }
          )
        }
      )
    })
  }

  async getIgUser(username) {
    return new Promise((res, rej) => {
      this.db.get(
        "SELECT * FROM instagramuser WHERE username = ?",
        username,
        (err, row) => {
          if (err) {
            rej(err)
          }
          _.isEmpty(row) ? res(null) : res(row)
        }
      )
    })
  }

  async getIgUsersFromTgChat(tgChatId) {
    const tgUser = await this.getTelegramUser(tgChatId)
    if (!tgUser) {
      return Promise.resolve(null)
    }
    return new Promise((res, rej) => {
      this.db.all(
        `
        SELECT a.username from instagramuser as a, telegramchatinstagramuser as b 
        WHERE a.id = b.instagramuserid
        AND b.telegramchatid = ?
      `,
        tgUser.id,
        (err, row) => {
          if (err) {
            rej(err)
          }
          res(row.map(el => el.username))
        }
      )
    })
  }

  async getAllIgUsers() {
    return new Promise((res, rej) => {
      this.db.all("SELECT DISTINCT username FROM instagramuser", (err, row) => {
        if (err) {
          rej(err)
        }
        res(row.map(el => el.username))
      })
    })
  }

  async deleteTelegramUser(tgChatId) {
    const tgUser = await this.getTelegramUser(tgChatId)
    return new Promise((res, rej) => {
      this.db.exec(
        `
      DELETE FROM telegramchatinstagramuser WHERE telegramchatid = ${tgUser.id};
      DELETE FROM telegramchat WHERE id = ${tgUser.id};
      DELETE FROM instagramuser WHERE id NOT IN (
        SELECT DISTINCT instagramuserid FROM telegramchatinstagramuser
      );`,
        err => {
          if (err) {
            rej(err)
          }
          res(true)
        }
      )
    })
  }

  async deleteIgUserFromTgChat(tgChatId, igUsername) {
    const tgUser = await this.getTelegramUser(tgChatId)
    const igUser = await this.getIgUser(igUsername)
    if (!tgUser || !igUser) {
      return Promise.reject(
        `Telegram ${JSON.stringify(tgUser)} or instagram user ${JSON.stringify(
          igUser
        )} not valid`
      )
    }
    return new Promise((res, rej) => {
      this.db.exec(
        `
        DELETE FROM telegramchatinstagramuser
        WHERE telegramchatinstagramuser.telegramchatid = ${tgUser.id}
        AND telegramchatinstagramuser.instagramuserid = ${igUser.id};
        DELETE FROM instagramuser 
        WHERE id NOT IN (
          SELECT DISTINCT instagramuserid from telegramchatinstagramuser
        )
      `,
        err => {
          if (err) {
            rej(err)
          }
          res(true)
        }
      )
    })
  }

  async getPolling(tgChatId) {
    return new Promise((res, rej) => {
      this.db.get(
        "SELECT polling FROM telegramchat WHERE chatid = ?",
        tgChatId,
        (err, row) => {
          if (err) {
            rej(err)
          }
          if (_.isEmpty(row)) {
            res(null)
          }
          res(row.polling)
        }
      )
    })
  }

  async setPolling(tgChatId, value) {
    return new Promise((res, rej) => {
      this.db.get(
        "UPDATE telegramchat SET polling = ? WHERE chatid = ?",
        [value, tgChatId],
        (err, row) => {
          if (err) {
            rej(err)
          }
          if (_.isEmpty(row)) {
            res(null)
          }
          res(true)
        }
      )
    })
  }

  async enablePolling(tgChatId) {
    await this.setPolling(tgChatId, 1)
  }

  async disablePolling(tgChatId) {
    await this.setPolling(tgChatId, 0)
  }

  async getTgUsersFromIgUser(igUsername, polling = null) {
    const igUser = await this.getIgUser(igUsername)
    const pollingString = polling ? `AND a.polling = ${polling}` : ``
    return new Promise((res, rej) => {
      this.db.all(
        `SELECT a.chatid, a.id FROM telegramchat as a, telegramchatinstagramuser as b
        WHERE b.instagramuserid = ? ${pollingString}`,
        igUser.id,
        (err, row) => {
          if (err) {
            rej(err)
          }
          _.isEmpty(row) ? res(null) : res(row)
        }
      )
    })
  }

  async getIgPost(link) {
    return new Promise((res, rej) => {
      this.db.get(
        "SELECT * FROM instagrampost WHERE link = ? LIMIT 1",
        link,
        (err, row) => {
          if (err) {
            rej(err)
          }
          _.isEmpty(row) ? res(null) : res(row)
        }
      )
    })
  }

  async insertIgPost(igUsername, link) {
    const igUser = await this.getIgUser(igUsername)
    if (!igUser) {
      return Promise.reject(`Username ${igUsername} not found in the db`)
    }
    return new Promise((res, rej) => {
      this.db.run(
        "INSERT INTO instagrampost VALUES (null, ?, ?, CURRENT_TIMESTAMP)",
        [link, igUser.id],
        async err => {
          if (err) {
            rej(err)
          }
          const igPost = await this.getIgPost(link)
          const tgUsers = await this.getTgUsersFromIgUser(igUsername)
          tgUsers.map(tgUser => {
            this.db.run(
              "INSERT INTO telegramchatinstagrampost (id, telegramchatid, instagrampostid) VALUES (null, ?, ?)",
              [tgUser.id, igPost.id],
              err => {
                if (err) {
                  rej(err)
                }
              }
            )
          })
          res(true)
        }
      )
    })
  }

  async getLastPostFromigUser(igUsername) {
    return new Promise((res, rej) => {
      this.db.get(
        `SELECT a.link FROM instagrampost as a, instagramuser as b 
      WHERE b.username = ? 
      ORDER BY a.createdtime DESC LIMIT 1`,
        igUsername,
        (err, row) => {
          if (err) {
            rej(err)
          }
          _.isEmpty(row) ? res(null) : res(row.link)
        }
      )
    })
  }

  async setPolledPost(tgChatId, link) {
    const tgUser = await this.getTelegramUser(tgChatId)
    const igPost = await this.getIgPost(link)
    if (!tgUser || !igPost) {
      return Promise.reject("Telegram user or link invalid")
    }
    return new Promise((res, rej) => {
      this.db.run(
        `UPDATE telegramchatinstagrampost SET polled = 1 WHERE telegramchatid = ? AND instagrampostid = ?`,
        [tgUser.id, igPost.id],
        err => {
          if (err) {
            rej(err)
          }
          res(true)
        }
      )
    })
  }
}

module.exports = DatabaseManager
