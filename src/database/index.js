const configs = require("../configs")
const dbManager = require("./databaseManager")
const db = new dbManager(configs)
module.exports = db
