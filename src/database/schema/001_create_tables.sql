CREATE TABLE `telegramchatinstagramuser` (
  `id`    INTEGER PRIMARY KEY AUTOINCREMENT,
  `telegramchatid`        INTEGER NOT NULL,
  `instagramuserid`       INTEGER NOT NULL,
  UNIQUE(`telegramchatid`, `instagramuserid`)
);
CREATE TABLE IF NOT EXISTS "instagrampost" (
  `id`    INTEGER PRIMARY KEY AUTOINCREMENT,
  `link`  TEXT NOT NULL UNIQUE,
  `instagramuserid`       INTEGER NOT NULL,
  `createdtime`   INTEGER DEFAULT CURRENT_TIMESTAMP
);
CREATE TABLE IF NOT EXISTS "instagramuser" (
  `id`    INTEGER PRIMARY KEY AUTOINCREMENT,
  `username`      TEXT NOT NULL UNIQUE
);
CREATE TABLE IF NOT EXISTS "telegramchatinstagrampost" (
  `id`    INTEGER PRIMARY KEY AUTOINCREMENT,
  `telegramchatid`        INTEGER NOT NULL,
  `instagrampostid`       INTEGER NOT NULL,
  `likes` INTEGER DEFAULT 0,
  `dislikes`      INTEGER DEFAULT 0,
  `polled`        INTEGER DEFAULT 0
);
CREATE TABLE IF NOT EXISTS "telegramchat" (
  `id`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `chatid`        INTEGER NOT NULL UNIQUE,
  `polling`       INTEGER NOT NULL DEFAULT 1
);