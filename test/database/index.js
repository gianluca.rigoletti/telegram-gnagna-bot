import test from "ava"
import dbManager from "../../src/database/databaseManager"
import configs from "./configs"
const db = new dbManager(configs)
import _ from "lodash"

test.before(async t => {
  // Create the database schema
  const sqlite3 = require("sqlite3")
  const db = new sqlite3.Database(configs.dbPath)
  console.log(`Creating the schema...`)
  return new Promise((res, rej) => {
    db.exec(
      `
  CREATE TABLE IF NOT EXISTS \`telegramchatinstagramuser\` (
    \`id\`    INTEGER PRIMARY KEY AUTOINCREMENT,
    \`telegramchatid\`        INTEGER NOT NULL,
    \`instagramuserid\`       INTEGER NOT NULL,
    UNIQUE(\`telegramchatid\`, \`instagramuserid\`)
  );
  CREATE TABLE IF NOT EXISTS "instagrampost" (
    \`id\`    INTEGER PRIMARY KEY AUTOINCREMENT,
    \`link\`  TEXT NOT NULL UNIQUE,
    \`instagramuserid\`       INTEGER NOT NULL,
    \`createdtime\`   INTEGER DEFAULT CURRENT_TIMESTAMP
  );
  CREATE TABLE IF NOT EXISTS "instagramuser" (
    \`id\`    INTEGER PRIMARY KEY AUTOINCREMENT,
    \`username\`      TEXT NOT NULL UNIQUE
  );
  CREATE TABLE IF NOT EXISTS "telegramchatinstagrampost" (
    \`id\`    INTEGER PRIMARY KEY AUTOINCREMENT,
    \`telegramchatid\`        INTEGER NOT NULL,
    \`instagrampostid\`       INTEGER NOT NULL,
    \`likes\` INTEGER DEFAULT 0,
    \`dislikes\`      INTEGER DEFAULT 0,
    \`polled\`        INTEGER DEFAULT 0
  );
  CREATE TABLE IF NOT EXISTS "telegramchat" (
    \`id\`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    \`chatid\`        INTEGER NOT NULL UNIQUE,
    \`polling\`       INTEGER NOT NULL DEFAULT 1
  );
  `,
      err => {
        if (err) {
          rej(err)
        }
        res()
      }
    )
  })
})

test("Database manager should have dbPath and db properties", async t => {
  const configs = {
    dbPath: "./test/database/gnagna.db"
  }
  const db = new dbManager(configs)
  t.not(db.dbPath, undefined)
  t.not(db.db, undefined)
})

test("dbManager.getAllTgUsers() should return null if no users exists", async t => {
  const result = await db.getAllTgUsers()
  t.is(result, null)
})

test("dbManager.insertTelegramUser(chatId) should insert a user and return the id", async t => {
  const resultId = await db.insertTelegramUser(12049120)
  const resultId2 = await db.insertTelegramUser(12039120)
  t.is(resultId, 1)
  t.is(resultId2, 2)
})

test("dbManager.getTelegramUser(chatId) should returns the corresponding record in the db", async t => {
  const result = await db.getTelegramUser(12049120)
  t.deepEqual(result, {
    id: 1,
    chatid: 12049120,
    polling: 1
  })
})

test("dbManager.getAllTgUsers() should return an array of users or null if no users exists", async t => {
  const result2 = await db.getAllTgUsers()
  t.deepEqual(result2, [12049120, 12039120])
})

test("dbManager.insertInstagramUser(tgChatId, igUser) should insert the record and return the id of the inserted user", async t => {
  const resultId = await db.insertInstagramUser(12049120, "dilettaleotta")
  const resultId2 = await db.insertInstagramUser(12049120, "olgachocolate")
  const resultId3 = await db.insertInstagramUser(12049120, "xenia")
  const resultId4 = await db.insertInstagramUser(12039120, "lindseypelas")
  const error = await t.throws(db.insertInstagramUser(null, "dilettaleotta"))
  t.truthy(error)
  const error2 = await t.throws(db.insertInstagramUser(12049120, null))
  t.truthy(error2)

  t.is(resultId, 1)
  t.is(resultId2, 2)
  t.is(resultId3, 3)
  t.is(resultId4, 4)
})

test("dbManager.getIgUsersFromTgChat(tgChatId) should return an array of instagram usernames", async t => {
  const igUsers = await db.getIgUsersFromTgChat(12049120)
  t.deepEqual(igUsers, ["dilettaleotta", "olgachocolate", "xenia"])
})

test("dbManager.getAllIgUsers() should return an array of all usernames in the database", async t => {
  const igUsers = await db.getAllIgUsers()
  t.deepEqual(igUsers, [
    "dilettaleotta",
    "olgachocolate",
    "xenia",
    "lindseypelas"
  ])
})

test("dbManager.deleteTelegramUser(tgChatId) should delete the record in the db and return true", async t => {
  await db.insertTelegramUser(12049728)
  await db.insertInstagramUser(12049728, "user1")
  await db.insertInstagramUser(12049728, "user2")
  await db.insertInstagramUser(12049728, "user3")
  const result = await db.deleteTelegramUser(12049728)
  t.true(result)
  const igUsers = await db.getIgUsersFromTgChat(12049728)
  t.deepEqual(igUsers, null)
})

test("dbManager.getIgUser(username) should return the instagramuser object from the db", async t => {
  const user1 = await db.getIgUser("nonexistingusername")
  t.is(user1, null)
  const user2 = await db.getIgUser("dilettaleotta")
  t.deepEqual(user2, {
    id: 1,
    username: "dilettaleotta"
  })
})

test("dbManager.deleteIgUserFromTgChat(tgChatId, igUser) should delete the users from the database and return true", async t => {
  await db.insertTelegramUser(12319728)
  await db.insertInstagramUser(12319728, "user1")
  await db.insertInstagramUser(12319728, "user2")
  await db.insertInstagramUser(12319728, "user3")
  const result = await db.deleteIgUserFromTgChat(12319728, "user1")
  t.is(result, true)
  const remainingUsers = await db.getIgUsersFromTgChat(12319728)
  t.deepEqual(remainingUsers, ["user2", "user3"])
  const result2 = await t.throws(
    db.deleteIgUserFromTgChat(12319728, "nonexistinguser")
  )
  t.truthy(result2)
  await db.deleteTelegramUser(12319728)
})

test("dbManager.enable/disable/get/Polling(tgChatId) should set and return the correct value of the polling", async t => {
  let polling1 = await db.getPolling(12049120)
  t.is(polling1, 1)
  await db.disablePolling(12049120)
  polling1 = await db.getPolling(12049120)
  t.is(polling1, 0)
  await db.enablePolling(12049120)
  polling1 = await db.getPolling(12049120)
  t.is(polling1, 1)
})

test("dbManager.getIgPost(link) should return null if there is no corresponding object in the db", async t => {
  const result = await db.getIgPost("https://www.instagram.com/p/BYw1TVGlzGe/")
  t.is(result, null)
})

test("dbManager.insertIgPost(igUsername, link) should insert a post and return true or reject the promise", async t => {
  // Attempt to insert into a non existing username should reject the promise
  const result = await t.throws(
    db.insertIgPost(
      "notexistingusername",
      "https://www.instagram.com/p/BYw1TVGlzGe/"
    )
  )
  t.truthy(result)
  const result2 = await db.insertIgPost(
    "dilettaleotta",
    "https://www.instagram.com/p/BYw1TVGlzGe/"
  )
  t.is(result2, true)
})

test("dbManager.getIgPost(link) should return the object in the db", async t => {
  const result = await db.getIgPost("https://www.instagram.com/p/BYw1TVGlzGe/")
  _.unset(result, "createdtime")
  t.deepEqual(result, {
    id: 1,
    instagramuserid: 1,
    link: "https://www.instagram.com/p/BYw1TVGlzGe/"
  })
})

test("dbManager.getLastPostFromiguser(igUsername) should return the last inserted link or null otherwise", async t => {
  const result = await db.getLastPostFromigUser("nonexisting")
  t.is(result, null)
  const result2 = await db.getLastPostFromigUser("dilettaleotta")
  t.is(result2, "https://www.instagram.com/p/BYw1TVGlzGe/")
})

test("dbManager.setPolledPost(tgChatId, link) should set polling to 1 or reject the promise", async t => {
  // Attempt to set a non existing telegram user
  await t.throws(
    db.setPolledPost(54090905, "https://www.instagram.com/p/BYw1TVGlzGe/")
  )
  // Attempt to set a non existing post
  await t.throws(
    db.setPolledPost(12049120, "https://www.instagram.com/p/nonexistingpost/")
  )
  const result = await db.setPolledPost(
    12049120,
    "https://www.instagram.com/p/BYw1TVGlzGe/"
  )
  t.is(result, true)
})

test.after.always(async t => {
  // Clean the database
  const sqlite3 = require("sqlite3")
  const db = new sqlite3.Database(configs.dbPath)
  console.log(`...dropping the tables`)
  db.exec(`
    DROP TABLE IF EXISTS \`telegramchat\`;
    DROP TABLE IF EXISTS \`instagrampost\`;
    DROP TABLE IF EXISTS \`instagramuser\`;
    DROP TABLE IF EXISTS \`telegramchatinstagrampost\`;
    DROP TABLE IF EXISTS \`telegramchatinstagramuser\`;
  `)
})
