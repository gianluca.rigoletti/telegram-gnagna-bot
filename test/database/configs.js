const test = require("ava")

const configs = {
  dbPath: "./test/database/gnagna.db"
}
module.exports = configs

test("Configs should always have a dbPath key", async t => {
  t.not(configs.dbPath, undefined)
})
